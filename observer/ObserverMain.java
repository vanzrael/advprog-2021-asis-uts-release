import adventurer.*;
import magic.*;
import java.util.Arrays;

public class ObserverMain {
	public static void main(String[] args){
		Adventurer[] advArr = {
			new DeathKnight("Argath Darksword"),
			new Duellist("Giorno Avola"),
			new Paladin("Falgath Lightsword"),
			new Warrior("Reinhard Braun")
		};

		AreaMagic[] magicArr = {
			new Curse("Demonic Presence", 10),
			new Haste("Stream of Star Explosion", 10),
			new Heal("Keyaruga's Heal", 10),
			new Strengthen("Tarukaja", 10)
		};

		int advLen = advArr.length;
		int magicLen = magicArr.length;

		for(int i = 0; i < magicLen; i++){
			for(int j = 0; j < advLen; j++){
				magicArr[i].addAdventurer(advArr[j]);
			}
		}

		for(int i = 0; i < magicLen; i++){
			System.out.println(magicArr[i].cast());
			for(int j = 0; j < advLen; j++){
				System.out.println(advArr[j]);
			}
		}
	}
}