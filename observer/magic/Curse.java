package magic;

public class Curse extends AreaMagic {
	public Curse(String name, int power){
		super(name, "curse", power);
	}
}