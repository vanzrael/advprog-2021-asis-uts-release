package magic;

public class Strengthen extends AreaMagic {
	public Strengthen(String name, int power){
		super(name, "strengthen", power);
	}
}