package adventurer;

// Kelas ini tidak boleh dimodifikasi
public class Adventurer implements Comparable<Adventurer>{
	private static int currentId = 0;

	private int id;
	private String name;
	private int hp;
	private int attack;
	private int speed;

	public Adventurer(String name, int hp, int attack, int speed){
		this.name = name;
		this.hp = hp;
		this.attack = attack;
		this.speed = speed;
		this.id = currentId;
		currentId++;
	}

	public String toString(){
		return String.format("%s | ID : %d, HP : %d, ATK : %d, SPD : %d", name, id, hp, attack, speed);
	}

	public int getId(){
		return id;
	}

	public String getName(){
		return name;
	}

	public int getHp(){
		return hp;
	}

	public int getAttack(){
		return attack;
	}

	public int getSpeed(){
		return speed;
	}

	@Override
	public int compareTo(Adventurer other){
		return id - other.id;
	}
}