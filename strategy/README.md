#Soal Latihan Strategy Pattern

Pada soal latihan ini, Anda diminta untuk mengimplementasikan Observer Pattern dengan benar.

## Requirements

Anda diminta untuk membuat implementasi pengurutan objek-objek `Adventurer` pada kelas `Guild` menggunakan Strategy Pattern. Terdapat 5 cara mengurutkan objek `Adventurer`, yaitu berdasarkan `id` (`sortById()`), berdasarkan `name` (`sortByName()`), berdasarkan `hp` (`sortByHp()`), berdasarkan `attack` (`sortByAttack()`), dan berdasarkan `speed` (`sortBySpeed()`). Kode pada kelas `Adventurer` tidak boleh dimodifikasi.

Hint : Manfaatkan *interface* `java.util.Comparator` dan `Collections.sort`

## Cara menjalankan program

1. Jalankan perintah `javac StrategyMain.java`
2. Jalankan perintah `java StrategyMain`
3. Output akan langsung dicetak oleh program