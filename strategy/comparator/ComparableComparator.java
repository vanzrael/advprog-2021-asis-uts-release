package comparator;

import java.util.Comparator;
import adventurer.Adventurer;

public class ComparableComparator implements Comparator<Adventurer> {
	@Override
	public int compare(Adventurer a1, Adventurer a2){
		return a1.compareTo(a2);
	}

	@Override
	public boolean equals(Object o){
		return this == o;
	}
}