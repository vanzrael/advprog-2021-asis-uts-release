import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

import java.util.StringTokenizer;
import java.util.Map;
import java.util.TreeMap;

import collection.Collection;
import collection.BackwardArray;
import collection.ForwardArray;
import collection.iterator.Iterator;
import object.Person;
import object.Property;
import object.Guild;

public class IteratorMain {
	public static void main(String[] args) throws IOException {
		Map<String, Person> personMap = new TreeMap<>();
		Collection<Person> personCollection = new ForwardArray<>();
		Collection<Property> propertyCollection = new BackwardArray<>();
		Collection<Guild> guildCollection = new BackwardArray<>();
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String str = in.readLine();
		while(str != null){
			StringTokenizer st = new StringTokenizer(str, ";");
			String objType = st.nextToken();
			if(objType.equals("person")){
				String personName = st.nextToken();
				String profession = st.nextToken();
				Person person = new Person(personName, profession);
				personMap.put(personName, person);
				personCollection.add(person);
			} else if(objType.equals("property")){
				String itemName = st.nextToken();
				String itemType = st.nextToken();
				String holderName = st.nextToken();
				Property property = new Property(itemName, itemType, personMap.get(holderName));
				propertyCollection.add(property);
			} else if(objType.equals("guild")){
				String guildName = st.nextToken();
				String masterName = st.nextToken();
				String memberString = st.nextToken();
				Person master = personMap.get(masterName);
				Guild guild = new Guild(guildName, master);
				master.setGuild(guild);
				StringTokenizer memberSt = new StringTokenizer(memberString, ",");
				while(memberSt.hasMoreTokens()){
					String memberName = memberSt.nextToken();
					Person person = personMap.get(memberName);
					if(person != null){
						guild.addMember(person);
						person.setGuild(guild);
					}
				}

				guildCollection.add(guild);
			}

			str = in.readLine();			
		}

		Iterator<Person> personIterator = personCollection.getIterator();
		Iterator<Property> propertyIterator = propertyCollection.getIterator();
		Iterator<Guild> guildIterator = guildCollection.getIterator();

		while(personIterator.hasNext()){
			System.out.println(personIterator.next());
		}
		System.out.println();

		while(propertyIterator.hasNext()){
			System.out.println(propertyIterator.next());
		}
		System.out.println();

		while(guildIterator.hasNext()){
			System.out.println(guildIterator.next());
		}
	}
}