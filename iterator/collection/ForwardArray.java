package collection;

import collection.iterator.Iterator;
import collection.iterator.DecoyIterator;

public class ForwardArray<T> extends CustomArray<T> {
	public ForwardArray(){
		super();
	}

	// method ini harus mengembalikan iterator yang memulai iterasi dari indeks 0
	// next = current index + 1
	@Override
	public Iterator<T> getIterator(){
		return new DecoyIterator<>();
	}
}