package collection;

import collection.iterator.Iterator;
import collection.iterator.DecoyIterator;

public class BackwardArray<T> extends CustomArray<T> {
	public BackwardArray(){
		super();
	}

	// method ini harus mengembalikan iterator yang memulai iterasi dari indeks <size> - 1
	// next : current index - 1
	@Override
	public Iterator<T> getIterator(){
		return new DecoyIterator<>();
	}
}