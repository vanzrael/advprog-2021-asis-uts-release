package object;

import collection.Collection;
import collection.ChainedQueue;
import collection.iterator.Iterator;

public class Guild {
	private String name;
	private Person guildmaster;
	private Collection<Person> members;

	public Guild(String name, Person guildmaster){
		this.name = name;
		this.guildmaster = guildmaster;
		this.members = new ChainedQueue<>();
	}

	public String getName(){
		return name;
	}

	public void addMember(Person person){
		members.add(person);
	}

	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("Guild name : %s", name)).append("\n");
		sb.append(String.format("Guildmaster : %s", guildmaster.getName())).append("\n");
		sb.append("Members : \n");
		Iterator<Person> memberIterator = members.getIterator();
		while(memberIterator.hasNext()){
			sb.append("\t").append(memberIterator.next()).append("\n");
		}
		sb.append("=======================");
		return sb.toString();
	}
}