package object;

public class Property {
	private String name;
	private String type;
	private Person holder;

	public Property(String name, String type, Person holder){
		this.name = name;
		this.type = type;
		this.holder = holder;
	}

	public String getName(){
		return name;
	}

	@Override
	public String toString(){
		return String.format("property name : %s, property type : %s, holder name : %s", name, type, holder.getName());
	}
}