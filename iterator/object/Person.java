package object;

public class Person {
	private String name;
	private String profession;
	private Guild guild;

	public Person(String name, String profession){
		this.name = name;
		this.profession = profession;
		this.guild = null;
	}

	public String getName(){
		return name;
	}

	public void setGuild(Guild guild){
		this.guild = guild;
	}

	@Override
	public String toString(){
		return String.format("name : %s, guild : %s, profession : %s", name, guild.getName(), profession);
	}
}