# Soal Latihan Iterator Pattern

Pada soal latihan kali ini, Anda diharapkan bisa mengimplementasikan Iterator Pattern dengan benar

## Requirements
Pada soal kali ini, terdapat sebuah *interface* struktur data bernama `Collection` yang terletak pada *package* `collection`. Kelas `Collection` memiliki tiga buah *method*, yaitu `add(T elem)`, `isEmpty()`, dan `getIterator()`. Saat ini, *method* `add(T elem)` dan `isEmpty()` sudah memiliki implementasi namun *method* `getIterator()` belum diimplementasikan dengan benar. Anda diharapkan untuk dapat mengimplementasikan *method* tersebut dengan memanfaatkan Iterator Pattern.

## Cara Menjalankan Program
1. Jalankan perintah `javac IteratorMain.java`
2. Jalankan perintah `java IteratorMain < input.txt`
3. Output akan langsung dicetak oleh program