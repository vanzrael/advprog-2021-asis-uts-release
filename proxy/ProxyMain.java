import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import core.NameCounterInstance;

public class ProxyMain {
    public static void main(String[] args) throws IOException {
        NameCounterInstance instance = new NameCounterInstance();

		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String command = "";
        while (!command.equals("exit")) {
            System.out.println("Heya. Choose one of the options below:");
            System.out.println("(1) Register name. (2) Get name by your entry number. " +
                               "(3) Count your name. (4) Get all names. (exit) Exit.");
            command = in.readLine();
            if (command.equals("1")) {
                System.out.print("Insert your name: ");
                instance.registerName(in.readLine());
            } else if (command.equals("2")) {
                System.out.print("Insert your entry number: ");
                try {
                    instance.getName(Integer.parseInt(in.readLine()));
                } catch (NumberFormatException e) {
                    System.out.println("Not an integer.");
                }
            } else if (command.equals("3")) {
                System.out.print("Insert your name: ");
                instance.countName(in.readLine());
            } else if (command.equals("4")) {
                System.out.print("Insert your name: ");
                instance.getAllNames(in.readLine());
            } else if (!command.equals("exit")) {
                System.out.println("Invalid argument.");
            }
            System.out.println();
        }
    }
}
