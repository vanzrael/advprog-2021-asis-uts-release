package repository;

import java.util.List;
import java.util.ArrayList;

public class NameRepository {
    private List<String> names;
    private static final NameRepository instance = new NameRepository();

    private NameRepository() {
        this.names = new ArrayList<>();
    }

    public static NameRepository getInstance() {
        return NameRepository.instance;
    }

    public int addName(String name) {
        this.names.add(name);
        return this.names.size();
    }

    public List<String> findAll() {
        return names;
    }

    public String findByIndex(int index) {
        try {
            return names.get(index);
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    public int countName(String name) {
        int nameCount = 0;
        for (String savedName: names) {
            if (savedName.equals(name)) {
                nameCount++;
            }
        }
        return nameCount;
    }

    public int getNameFirstIndex(String name) {
        return names.indexOf(name);
    }
}
