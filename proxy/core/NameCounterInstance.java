package core;

import java.util.UUID;
import repository.NameRepository;

public class NameCounterInstance {
    private NameRepository repository;
    private String serverUuid;

    public NameCounterInstance() {
        this.repository = NameRepository.getInstance();
        this.serverUuid = UUID.randomUUID().toString();
    }

    public String getServerUuid() {
        return serverUuid;
    }

    public void registerName(String name) {
        int entryNumber = this.repository.addName(name);
        System.out.println("Heya, this is server " + this.serverUuid + " talking!");
        System.out.println("Your name is successfully registered.");
        System.out.println("There are " + Integer.toString(entryNumber - 1) + " people registered before you.");
    }

    public void getAllNames(String name) {
        int firstIndex = this.repository.getNameFirstIndex(name);
        System.out.println("Heya, this is server " + this.serverUuid + " talking!");
        if (firstIndex == 0) {
            System.out.println("Because you are the first one registering, I can give you all the registered names.");
            System.out.println("This... gonna be a long one.");
            int index = 0;
            for (String registeredName: this.repository.findAll()) {
                System.out.println(Integer.toString(index) + ": " + registeredName);
                index++;
            }
        } else {
            System.out.println("Hmm... you didn't have the privilege required. Be faster next time.");
        }
    }

    public void getName(int entryNumber) {
        String name = this.repository.findByIndex(entryNumber);
        System.out.println("Heya, this is server " + this.serverUuid + " talking!");
        if (name != null) {
            System.out.println("Thanks for giving me your entry number.");
            System.out.println("You are... " + name + ", aren't you?");
        } else {
            System.out.println("Err... I don't think this number is valid.");
        }
    }

    public void countName(String name) {
        int count = this.repository.countName(name);
        System.out.println("Heya, this is server " + this.serverUuid + " talking!");
        System.out.println("There are " + Integer.toString(count) + " people whose name is " + name);
    }
}
