# Kumpulan Soal Latihan Analisis

Direktori ini berisi kumpulan soal studi kasus terkait materi perkuliahan UTS Advanced Programming (mostly terkait Design Pattern).
Kami tidak menyediakan *source code* dalam bentuk apapun pada tipe soal ini, kecuali disebutkan sebaliknya.

Pada setiap soal, kalian akan diminta untuk melakukan analisis terhadap suatu studi kasus dan menentukan bagaimana sebaiknya implementasi penyelesaian masalah tersebut dilakukan, dengan memanfaatkan **_design pattern_** dan penerapan materi lainnya (*refactoring*, dsb.) dengan tepat.
Sebagian soal mungkin juga akan meminta kalian menuliskan jawaban kalian melalui file Markdown terpisah (misal: `notes.md`), atau meminta kalian membuat implementasi Java dari solusi yang kalian *propose*.

Selamat berlatih dan semoga latihan-latihan ini bisa *improve* kemampuan analisis kalian.
