package weapon;

public class Weapon {
	private String name;
	private int atk;
	private int acc;
	private int crit;
	private int eva;
	private int def;

	public Weapon(String name, int atk, int acc, int crit, int eva, int def){
		this.name = name;
		this.atk = atk;
		this.acc = acc;
		this.crit = crit;
		this.eva = eva;
		this.def = def;
	}

	public int getATK(){
		return atk;
	}

	public int getACC(){
		return acc;
	}

	public int getCRIT(){
		return crit;
	}

	public int getEVA(){
		return eva;
	}

	public int getDEF(){
		return def;
	}
}