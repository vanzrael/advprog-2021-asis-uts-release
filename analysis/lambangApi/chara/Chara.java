package chara;

/*
* TO DO : Lengkapi fitur-fitur yang belum lengkap (baca fitur-fitur pada README) 
*/
public class Chara {
	private String name;
	private Weapon weapon;
	private int hp;
	private int str;
	private int dex;
	private int agi;
	private int arm;

	public Chara(String name, Weapon weapon, int hp, int str, int dex, int agi, int arm){
		this.name = name;
		this.weapon = weapon;
		this.hp = hp;
		this.str = str;
		this.dex = dex;
		this.agi = agi;
		this.arm = arm;
	}

	public String getName(){
		return name;
	}

	public void setWeapon(Weapon weapon){
		this.weapon = weapon;
	}

	public int getHp(){
		return hp;
	}

	public void heal(int hpAmmount){
		this.hp += hpAmmount;
	}

	public void damage(int hpAmmount){
		this.hp -= hpAmmount;
	}

	public int getATK(){
		return weapon.getATK() + str;
	}

	public int getACC(){
		return weapon.getACC() + dex * 10;
	}

	public int getCRIT(){
		return weapon.getCRIT() + dex;
	}

	public int getEVA(){
		return weapon.getEVA() + (int)(Math.ceil(spd * 8.5));
	}

	public int getDEF(){
		return weapon.getDEF() + arm;
	}
}