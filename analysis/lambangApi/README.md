# Lambang Api : Sebuah Permainan Strategi Bermain Peran
Pada soal kali ini, Anda diminta untuk mengimplementasikan beberapa mekanik sederhana pada sebuah permainan strategi bermain peran (Strategy Role Playing Game) bernama Lambang Api.

## Penjelasan Requirements (Fitur)
Pada bagian ini dijelaskan kebutuhan aplikasi yang harus Anda implementasikan. Harap membaca dengan teliti dan seksama penjelasan untuk masing-masing mekanik.

### Penjelasan Statistik Karakter (sudah diimplementasi)
Setiap karakter pada permainan ini memiliki statistik dalam bentuk atribut-atribut. Atribut-atribut tersebut adalah sebagai berikut:
* `HP` : Jumlah *hit point* yang dimiliki oleh karakter. Jika `HP` dari karakter turun sampai mencapai nilai `0`, maka karakter tersebut dinyatakan kalah
* `STR` : Atribut yang menentukan seberapa besar *damage* dari serangan yang dilakukan oleh karakter
* `DEX` : Atribut yang mempengaruhi nilai *damage*, akurasi dari serangan (serangan tepat sasaran atau tidak), dan juga kemungkinan karakter melakukan *critical attack*
* `AGI` : Atribut yang mempengaruhi kemungkinan karakter menghindari serangan karakter lain dan jika memiliki nilai cukup, menyerang dua kali pada fase pertarungan yang sama
* `ARM` : Atribut yang mempengaruhi besarnya nilai *damage* yang akan diterima karakter saat karakter terebut diserang oleh karakter lain.

### Penjelasan Statistik Menyerang dan Bertahan (sudah diimplementasi)
Selain statistik untuk karakter, terdapat statistik lain yang digunakan untuk menyerang dan bertahan. Statistik tersebut adalah sebagai berikut :
* `ATK` : statistik yang menyatakan seberapa besar nilai *damage* pada suatu serangan yang dilakukan suatu karakter sebelum dikurangi oleh statistik karakter bertahan. Kalkulasi : `STR`
* `ACC` : Kemungkinan (dalam persen) serangan yang dilakukan karakter tepat sasaran sebelum dikurangi oleh statistik karakter bertahan. Kalkulasi : `DEX` * `10`
* `CRIT` : Kemungkinan (dalam persen) suatu karakter melakukan aktivasi mekanisme menyerang. Kalkulasi : `DEX`
* `EVA` : Kemungkinan (dalam persen) suatu karakter menghindari serangan dari karakter lain. Kalkulasi : `SPD` * `8.5` (bulatkan menggunakan fungsi *ceiling*)
* `DEF` : statistik untuk mengurangi nilai *damage* yang diterima oleh suatu karakter saat karakter tersebut diserang oleh karakter lain. Kalkulasi : `ARM`

### Penjelasan Mengenai Senjata (sudah diimplementasi)
Masing-masing karakter dapat menggunakan satu senjata. Senjata yang digunakan oleh karakter akan digunakan untuk menyerang dan bertahan. Senjata memiliki atribut berupa statistik menyerang dan bertahan (`ATK`, `ACC`, `CRIT`, `EVA`, `DEF`) dan nilai-nilai dari masing-masing atribut akan ditambahkan ke dalam nilai statistik menyerang dan bertahan yang dimiliki karakter setelah kalkulasi. Karakter dapat mengganti senjata sebelum atau sesudah fase pertarungan.

### Penjelasan Mengenai Fase Pertarungan
Fase pertarungan terdiri dari 3 kali penyerangan. Penyerangan pertama dilakukan oleh suatu karakter (selanjutnya disebut sebagai A) kepada karakter lain (selanjutnya sebagai B). Penyerangan kedua dilakukan oleh B kepada A. Jika selisih nilai `AGI` antara A dan B lebih besar atau sama dengan `4`, maka karakter dengan nilai `AGI` lebih besar akan melakukan serangan kepada karakter dengan nilai `AGI` lebih kecil.

Pada setiap penyerangan yang dilakukan oleh suatu karakter kepada karakter lain, karakter penyerang akan mengaktifkan mekanisme menyerang sedangkan karakter bertahan akan mengaktifkan mekanisme bertahan. Penjelasan untuk masing-masing mekanisme menyerang dan bertahan dijelaskan di bagian berikutnya.

### Penjelasan Mekanisme Menyerang
Mekanisme menyerang bertujuan untuk menghitung nilai `DMG`. Nilai `DMG` total akan menjadi parameter untuk eksekusi
Berikut penjelasan masing-masing mekanisme serangan : 
* `Basic Attack` : Melakukan serangan biasa. `DMG` = `ATK`
* `Critical Attack` : Jika memenuhi kondisi *trigger*, maka akan melakukan *critical attack*. `trigger` : `randomNumber[0,99]` < `CRIT`. `multiplier` = `trigger` ? `3` : `1`. `DMG` = *damage* yang dihasilkan oleh mekanisme menyerang selanjutnya * `multiplier`. 
* `Shooting Star` : Melakukan lima serangan berturut-turut jika memenuhi kondisi *trigger*. `chance` = (`ACC` + `CRIT`) * `0.1` (gunakan fungsi *ceiling* untuk pembulatan). `trigger` : `chance` >= `100` or `randomNumber[0,99]` < `chance`. `attempt` = `trigger` ? 5 : 1. `DMG` = *total damage* dari mekanisme menyerang selanjutnya yang dilakukan `attempt` kali (nilai *damage* dari mekanisme menyerang bisa berbeda-beda karena faktor acak) 
* `Morning Star` : Menyembuhkan karakter penyerang (menambahkan nilai `HP`) sebanyak `DMG` jika memenuhi kondisi *trigger*. `chance` : (`ATK` * `2` + `DEF`) * `0.1` (gunakan fungsi *ceiling* untuk pembulatan). `trigger` : `chance` >= `100` or `randomNumber[0,99]` < `chance`. `DMG` = *damage* dari mekanisme menyerang selanjutnya.
* `Void Star` : Mengabaikan `DEF` yang dimiliki oleh karakter bertahan saat menyerang jika memenuhi kondisi *trigger*. `trigger` :`randomNumber[0,99]` < `CRIT`. `DMG` = *damage* yang dihasilkan oleh mekanisme menyerang selanjutnya + `DEF` karakter bertahan.

### Penjelasan Mekanisme Bertahan
* `Basic Defense` : Pertahanan biasa. Mengurangi nilai *damage* yang diterima dengan `DEF`. Efek : `HP` := `HP` - (`DMG` - `DEF`)
* `Dodge` : Menghindari serangan sehingga `HP` tidak berkurang sedikitpun jika memenuhi kondisi *trigger*. `diff` = `EVA` - `ACC` penyerang. `chance` = `diff` < `0` ? `diff`. `trigger` = `randomNumber[0,99]` < `chance`. Efek : if `trigger` -> `DMG` := `0`, else -> lanjut ke mekanisme bertahan selanjutnya.
* `Parry Riposte` : Menangkis serangan musuh dan melakukan serangan balik yang cepat jika memenuhi kondisi *trigger*. `trigger` : `randomNumber[0,99]` < (`ACC` + `CRIT`). Efek : if `trigger`, `HP` penyerang := `HP` penyerang - `ATK`; `DMG` := 0, else -> lanjut ke mekanisme bertahan selanjutnya. (Notes : serangan balik tidak akan ditangani oleh mekanisme bertahan musuh)
* `Absolute Defense` : Membuat *buffer* untuk *damage* dengan nilai awal `DEF` * `3`. *Buffer* akan terus berkurang setiap kali menerima *damage*. Efek : if `buffer` < 0 -> lanjut ke mekanisme bertahan selanjutnya, else -> `buffer` := `buffer` - `DMG`
* `Absorb Damage` : Menyembuhkan karakter jika memenuhi kondisi *trigger*. `chance` : (`ATK` + `DEF` * `2`) * `0.1` (gunakan fungsi *ceiling* untuk pembulatan). 

### Penjelasan Mengenai Pemilihan Mekanisme Menyerang dan Bertahan
Pemilihan mekanisme menyerang dan mekanisme bertahan dilakukan saat objek `Chara` dibuat (lihat `chara/Chara.java`). Masing-masing karakter dapat memilih 2 mekanisme berbeda masing-masing untuk menyerang dan bertahan dan `Basic Attack` dan `Basic Defense` akan dimasukkan secara otomatis (`Basic Attack` dan `Basic Defense` tidak masuk ke dalam 2 mekanisme yang harus dipilih). Mekanisme dapat disusun sesuai dengan keinginan namun `Basic Attack` dan `Basic Defense` harus merupakan mekanisme terakhir pada masing-masing pilihan. (contoh : `Shooting Star` -> `Critical Attack` -> `Basic Attack` dan `Critical Attack` -> `Shooting Star` -> `Basic Attack` diperbolehkan namun `Critical Attack` -> `Basic Attack` -> `Shooting Star` tidak diperbolehkan). Pengguna tidak dapat mengubah baik mekanisme menyerang maupun mekanisme bertahan setelah pembuatan objek `Chara` berhasil dibuat.

### Penjelasan Aksi yang Dapat Dilakukan Karakter
Pada soal kali ini, terdapat 3 aksi yang bisa dilakukan karakter dan ketiganya harus diimplementasikan.
1. Aksi untuk memulai fase penyerangan
2. Aksi untuk melakukan *self-healing* : meningkatkan `HP` karakter sebesar `10`
3. Mengganti senjata (sudah diimplementasikan)

## Penjelasan Requirements (Rancangan/Design)
Saat ini, mekanik yang Anda harus implementasi di soal latihan ini tidak terlalu banyak namun tidak menutup kemungkinan Lambang Api akan berkembang menjadi permainan yang besar dan kompleks dan banyak fitur/mekanik yang akan ditambahkan di masa depan. Oleh karena itu, terdapat beberapa kebutuhan dari sisi rancangan/*design* yang harus dipenuhi oleh implementasi yang Anda buat.

1. Eksekusi aksi tidak boleh dilakukan secara *hard-coded* menggunakan `if`/`switch` (Hint : susun aksi-aksi sedemikian rupa sehingga pemanggilan dapat dilakukan dengan mudah)
2. Pastikan implementasi yang Anda buat mengizinkan penambahan mekanisme pada karakter tanpa harus melakukan modifikasi pada kode karakter maupun kode masing-masing mekanisme.

## Petunjuk Pengerjaan
1. Baca dan pahami *requirements* yang tertulis di *file* ini.
2. Silahkan tuliskan asumsi untuk hal-hal yang belum dijelaskan secara detail pada sebuah *file baru* bernama `NOTES.md`
3. Tentukan *design pattern* yang tepat. Anda mungkin ingin mengimplementasikan lebih dari satu *design pattern* (Silahkan tulis penjelasan solusi yang Anda buat di `NOTES.md` jika Anda ingin menjelaskan solusi Anda)
4. Buat *main program* untuk melakukan simulasi (program Java biasa saja sudah cukup)
5. Silahkan tambah/modifikasi kode bila diperlukan