# Soal Latihan Asynchronous
Pada soal latihan kali ini, Anda diminta untuk mengimplementasikan suatu fungsionalitas dari program secara asinkronus.

## Requirements
Ubah implementasi metode `asyncMain()` pada berkas `AsyncExample.java` sehingga proses konversi *file* ke objek Java (baik `Guild` maupun `GuildMember`) dapat dieksekusi secara asinkronus

## Cara menjalankan program
1. Jalankan perintah `javac AsyncExample.java`
2. Jalankan perintah `java AsyncExample async` jika proses konversi ingin dijalankan secara asinkronus
3. Jalankan perintah `java AsyncExample sync` jika proses konversi ingin dijalankan secara sinkronus
4. Output akan langsung dicetak oleh program