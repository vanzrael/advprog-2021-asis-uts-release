public class GuildMember {
	private String name;
	private Guild guild;
	private String occupation;

	public GuildMember(String name, Guild guild, String occupation){
		this.name = name;
		this.guild = guild;
		this.occupation = occupation;
	}

	public String getName(){
		return name;
	}

	@Override
	public String toString(){
		return String.format("Guild member :\n\t name : %s,\n\t guild : %s,\n\t occupation : %s\n=================", name, guild.getName(), occupation);
	}
}