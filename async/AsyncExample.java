import java.util.List;
import java.util.Map;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;

public class AsyncExample {
	public static void main(String[] args) throws Exception {
		if(args[0].equals("async")){
			asyncMain();
		} else {
			nonAsyncMain();
		}
	}

	public static void nonAsyncMain(){
		Map<String, Guild> guildMap = GuildSeedConverter.convertToGuilds("guildSeed.txt");
		List<GuildMember> memberList = GuildSeedConverter.convertToMembers("memberSeed.txt", guildMap);
		for(String guildName : guildMap.keySet()){
			System.out.println(guildMap.get(guildName));
		}

		for(GuildMember member : memberList){
			System.out.println(member);
		}

	}

	public static void asyncMain() throws Exception {
		// TO DO : dapatkan daftar guild dan daftar member secara asynchronous dan print saat sudah selesai
		// Hint : gunakan kelas CompletableFuture, baca Javadocs terkait kelas tersebut
		// Notes : silahkan tambahkan/ganti behaviour sesuai kebutuhan 
		// (misalnya : tidak melakukan print namun membuat file baru berupa .json)
		Map<String, Guild> guildMap = GuildSeedConverter.convertToGuilds("guildSeed.txt");
		List<GuildMember> memberList = GuildSeedConverter.convertToMembers("memberSeed.txt", guildMap);

		for(String guildName : guildMap.keySet()){
			System.out.println(guildMap.get(guildName));
		}

		for(GuildMember member : memberList){
			System.out.println(member);
		}

		// TO DO :
		// * ganti kondisi guard loop supaya tetap looping saat operasi async di atas belum selesai
		// * dan berhenti looping saat operasi async di atas sudah selesai
		// * Hint : baca Javadocs CompletableFuture
		int i = 0;
		while(i < 100){
			System.out.println("Now loading");
			System.out.println("i = " + i);
			i++;
		}	
	}
}