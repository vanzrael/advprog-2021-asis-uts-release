import java.util.List;
import java.util.ArrayList;

public class Guild {
	private String name;
	private GuildMember guildmaster;
	private String location;
	private char rank;
	private List<GuildMember> members;

	public Guild(String name, String masterName, char locationCh, char rank){
		this.name = name;
		this.rank = rank;
		this.location = locationCharToStr(locationCh);
		this.members = new ArrayList<>();
		this.guildmaster = new GuildMember(masterName, this, "guildmaster");
	}

	public String getName(){
		return name;
	}

	public void addMember(GuildMember member){
		this.members.add(member);
	}

	@Override
	public String toString(){
		return String.format("Guild :\n\t name : %s,\n\t guildmaster : %s,\n\t location : %s,\n\t rank : %s\n===================", name, guildmaster.getName(), location, "" + rank);
	}

	private static String locationCharToStr(char locationCh){
		String format = "%s Azalia";
		String prefix;
		if(locationCh == 'N'){
			prefix = "North";
		} else if(locationCh == 'E'){
			prefix = "East";
		} else if(locationCh == 'S'){
			prefix = "South";
		} else {
			prefix = "West";
		}

		return String.format(format, prefix);
	}
}