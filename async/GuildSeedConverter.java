import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class GuildSeedConverter {
	public static Map<String, Guild> convertToGuilds(String filename){
		Map<String, Guild> res = new HashMap<>();
		try {
			BufferedReader in = new BufferedReader(new FileReader(filename));
			String str = in.readLine();
			while(str != null){
				StringTokenizer st = new StringTokenizer(str, ";");
				String guildName = st.nextToken();
				String masterName = st.nextToken();
				char locationCh = st.nextToken().charAt(0);
				char guildRank = st.nextToken().charAt(0);
				res.put(guildName, new Guild(guildName, masterName, locationCh, guildRank));
				str = in.readLine();
			}

			in.close();
		} catch(IOException ioe){
			ioe.printStackTrace();
		}
		

		return res;
	}

	public static List<GuildMember> convertToMembers(String filename, Map<String, Guild> guildList) {
		List<GuildMember> res = new ArrayList<>();
		try {
			BufferedReader in = new BufferedReader(new FileReader(filename));
			String str = in.readLine();
			while(str != null){
				StringTokenizer st = new StringTokenizer(str, ";");
				String memberName = st.nextToken();
				String guildName = st.nextToken();
				String occupation = st.nextToken();
				if(guildList.containsKey(guildName)){
					Guild guild = guildList.get(guildName);
					GuildMember member = new GuildMember(memberName, guild, occupation);
					guild.addMember(member);
					res.add(member);
				}
				str = in.readLine();
			}

			in.close();
		} catch(IOException ioe){
			ioe.printStackTrace();
		}

		return res;
	}
}