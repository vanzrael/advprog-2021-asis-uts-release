
# Sistem Informasi Yayasan Amal (SIYA)
~~https://amanah.cs.ui.ac.id spinoff, it might be~~

Usep adalah seorang *programmer* handal yang cukup dikenal warga kecamatan Gunungroto.
Suatu hari, rumah Usep didatangi oleh seseorang dari suatu yayasan amal.
Biasanya, Usep mungkin tidak akan terlalu menggubris mereka karena umumnya mereka meminta donasi dari rumah ke rumah.
Akan tetapi, kali ini Usep ingin tahu apa yang diinginkan dari seseorang itu.

Seseorang tersebut mengetok pagar, "Permisi."

Usep membuka pintu lalu berkata, "Ada yang bisa saya bantu?"

Seseorang tersebut berkata, "Kami dari yayasan Peduli Hewan Liar yang kantornya ada di seberang jalan besar komplek Anda.
Kami merupakan yayasan yang menyediakan *shelter* untuk hewan-hewan liar.... hewan yang ada di jalanan, untuk membantu mereka mendapatkan perhatian yang baik sebelum diadopsi oleh orang lain.
Kami di sini bukan untuk meminta donasi, tetapi kami membutuhkan Anda untuk keperluan lain."

Dia pun menyerahkan sebuah berkas proposal sembari melanjutkan, "Jadi, selama ini, dalam mencatat dan melaporkan kegiatan serta *cash flow* yayasan kami, kami masih melakukannya secara manual dengan bantuan Microsoft Excel.
Akan tetapi, setelah dua tahun terakhir, yayasan kami berkembang pesat seiring meluasnya berita terkait keberadaan kami.
Sehingga, kami membuka 11 cabang di kabupaten Bujur untuk melayani permintaan tersebut, dan pendataan manual membuat kami cukup kewalahan.
Selain itu, sarana publikasi juga jadi kurang teratur karena masih melalui kontak WA masing-masing staf kami."

Usep kemudian mengizinkan seseorang tersebut masuk ke halaman depan rumah.

## Requirements
"Oh iya, saya lupa memperkenalkan diri. Nama saya Ahmad. Saya kenal bapak Usep karena bapak Usep cukup populer di sini sebagai *programmer* yang handal. Oleh karena itu, kami ingin meminta pendapat Bapak terkait situs sistem informasi yang ingin kami bangun."

Kemudian dia melanjutkan, "Jadi, mungkin baiknya saya langsung jelaskan saja kebutuhannya."
Kemudian dia menuliskan daftar *use case* yang bisa dilakukan di kertas tersebut.

Sistem informasi ini nantinya akan digunakan oleh 5 *stakeholder*, yaitu:
- staf dari yayasan Peduli Hewan Liar,
- masyarakat pengadopsi hewan liar (*adopter*),
- donatur,
- masyarakat umum, dan
- perwakilan dari pemerintah atau BAZNAS.

Kemudian, ini adalah daftar *use case* yang bisa dimiliki oleh sistem informasi yayasan tersebut:
- Staf bisa melihat profil pribadi semua orang
- Semua orang bisa melakukan pembaruan profil pribadi
- Staf bisa menulis profil yayasan
- Staf bisa mendaftarkan cabang baru
- Masyarakat umum bisa melihat daftar cabang beserta deskripsi dan kontak cabang
- Staf bisa mendaftarkan program kerja baru, yang berlaku baik di cabang maupun secara keseluruhan
- Masyarakat umum bisa melihat program kerja yang dilaksanakan oleh yayasan
- Staf bisa menulis artikel blog
- Masyarakat umum bisa melihat daftar dan artikel blog
- Masyarakat umum bisa melaporkan hewan liar atau penyiksaan terhadap hewan liar kepada yayasan
- Staf bisa melihat laporan hewan liar atau penyiksaan terhadap hewan liar dari masyarakat
- Staf bisa mengganti status laporan hewan liar atau penyiksaan terhadap hewan liar dari masyarakat (dilihat, diperiksa, diproses, ditangani, selesai)
- Staf bisa mendaftarkan hewan liar yang sudah ditampung di *shelter* masing-masing cabang
- *Adopter* bisa melihat daftar hewan liar yang sudah ditampung di *shelter* di cabang terdekat
- *Adopter* bisa melakukan pengajuan adopsi hewan liar
- Staf bisa melihat dan melakukan *approval* untuk pengajuan adopsi hewan liar
- Staf bisa mendaftarkan *merchandise* baru
- Staf bisa memperbarui stok *merchandise*
- Masyarakat umum bisa melihat deskripsi *merchandise*
- Masyarakat umum bisa memasukkan *merchandise* ke dalam *cart*
- Masyarakat umum bisa melihat, mengganti kuantitas, dan menghapus barang dari *cart*
- Masyarakat umum bisa melanjutkan pembelian *merchandise* yang sudah dimasukkan ke dalam *cart* melalui ATM bank, OWO, atau TokekPay
- Donatur bisa melakukan donasi melalui ATM bank, OWO, atau TokekPay
- Donatur bisa melakukan konfirmasi donasi
- Donatur bisa melihat daftar donasi yang telah dilakukan dan statusnya
- Staf bisa melihat donasi yang dilakukan donatur
- Staf bisa menerima (*approve*) atau menolak (*reject*) konfirmasi donasi yang dilakukan donatur
- Staf bisa melakukan pendataan pemasukan dari donasi atau *merchandise*
- Staf bisa melakukan pendataan pemasukan dari program kerja
- Masayarakat umum bisa melihat rangkuman pemasukan yayasan, berdasarkan program kerja atau secara umum
- Staf bisa melakukan pendataan pengeluaran secara umum
- Staf bisa melakukan pendataan pengeluaran dari program kerja
- Masyarakat umum bisa melihat rangkuman pengeluaran yayasan, berdasarkan program kerja atau secara umum
- Perwakilan BAZNAS bisa melihat laporan keuangan dengan standar PSAK-45 berdasarkan pemasukan dan pengeluaran yayasan

Usep pun tertegun, "Cukup banyak ya. Mungkin ada penjelasan lain yang ingin pak Ahmad sampaikan?"

Ahmad pun melanjutkan, "Kami sebenarnya sudah melakukan *hiring* beberapa *programmer* untuk mengerjakan sistem informasi ini.
Namun, yang kami butuh pendapat dari pak Usep adalah, kami ingin tahu terkait arsitektur dari sistem ini.
Sistem ini nantinya perlu di-*scale* karena pemerintah Java Barat telah menghubungi kami dan tertarik untuk mendanai yayasan kami, sehingga yayasan kami akan tersebar di seluruh Java Barat.
Selain itu, *programmer* yang berhasil kami *hire* ternyata memiliki latar belakang *tech stack* yang berbeda-beda, sehingga kami ingin menjaga kebebasan mereka untuk memilih *tech stack* tersebut."

Usep pun berpikir sambil tertawa kecil, "Hmm... mungkin baiknya kita bisa memanfaatkan konsep *microservices* untuk menyusun arsitektur sistem informasi ini.
Akan tetapi, izinkan saya untuk mengelompokkan sekian banyak *use case* yang Bapak tuliskan ini :D"

## TO-DO
Sekarang, Anda sebagai "anak angkat" pak Usep bisa membantu pak Usep sebagai berikut:
1. [ ] Kelompokkan *use case* tersebut ke dalam *domain*. Mungkin kalau Anda sempat, bisa buat relasi *class* nya juga untuk menentukan bagian mana yang *coupled*.
2. [ ] Setelah mengelompokkan berdasarkan *domain*, tentukan *service* apa saja yang dibentuk apabila sistem ini menggunakan *microservices*.
3. [ ] Setelah menentukan *service*, tentukan juga bagian mana yang perlu lebih di-*scale*.
4. [ ] Tentukan juga bagaimana baiknya yayasan ini melakukan *deployment* terhadap *service instance* yang nanti dibangun.
5. [ ] Menurut Anda, apakah nanti desain yang Anda bangun perlu disesuaikan juga dengan bantuan *profiling*? Jelaskan pula alasannya.

## Answers
**Tuliskan jawaban anda di sini.**
