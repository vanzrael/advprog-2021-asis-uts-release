package duellist;

import attack.Attack;
import stance.Stance;
import stance.Engarde;
import stance.misc.StancePair;
import stance.misc.StanceRepository;

import java.util.List;

public class Duellist {
	private String name;
	private Stance currentStance;

	public Duellist(String name){
		this.name = name;
		currentStance = StanceRepository.ENGARDE;
	}

	@Override
	public String toString(){
		return String.format("Name : %s, current stance : %s", name, currentStance);
	}

	public boolean changeStance(Stance stance){
		boolean changeValid = currentStance.containsStance(stance);
		if(changeValid){
			currentStance = stance;
		}

		return changeValid;
	}

	public String getName(){
		return name;
	}

	public List<Stance> getAvailableStances(){
		return currentStance.getAvailableStances();
	}

	public List<Attack> getAvailableAttacks(){
		return currentStance.getAvailableAttacks();
	}

	public boolean hasLost(){
		return currentStance.getStanceType().equals("Lost");
	}

	public boolean attackDuellist(Attack attack, Duellist other){
		boolean attackValid = currentStance.containsAttack(attack);
		if(attackValid){
			Stance nextStance = other.defend(attack);
			currentStance = nextStance;
		}

		return attackValid;
	}

	/**
	* @return stance untuk Duellist yang menyerang
	*/
	private Stance defend(Attack attack){
		// TO DO : Manfaatkan variabel defendResult dan
		// kelas StancePair untuk mendapatkan return value yang diinginkan
		StancePair defendResult = new StancePair(StanceRepository.ENGARDE, StanceRepository.ENGARDE);
		if(attack == Attack.FAKE){
			// TO DO : Lengkapi bagian ini
		} else if(attack == Attack.LUNGE){
			// TO DO : Lengkapi bagian ini
		} else if(attack == Attack.RIPOSTE){
			// TO DO : Lengkapi bagian ini
		} else if(attack == Attack.SECOND_INTENTION){
			// TO DO : Lengkapi bagian ini
		} else {
			// TO DO : Lengkapi bagian ini
		}
		currentStance = defendResult.getDefenderStance();
		return defendResult.getAttackerStance();
	}
}