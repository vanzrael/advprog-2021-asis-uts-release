import duellist.Duellist;
import attack.Attack;
import stance.Stance;
import stance.misc.StanceRepository;

import java.util.Arrays;
import java.util.List;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class StateMain {
	public static void main(String[] args) throws IOException {
		Duellist avola = new Duellist("Giorgio Avola");
		Duellist garozzo = new Duellist("Daniele Garozzo");

		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		System.out.println(avola);
		System.out.println(garozzo);
		System.out.println();

		int i = 0;
		while(!avola.hasLost() && !garozzo.hasLost()){
			Duellist duellist1 = i % 2 == 0 ? avola : garozzo;
			Duellist duellist2 = i % 2 == 1 ? avola : garozzo;

			System.out.println(availableFormatStances(duellist1.getAvailableStances(), duellist1.getName()));
			Stance chosenStance = null;
			boolean validStance = false;
			while(!validStance){
				System.out.print("Please enter next stance : ");
				chosenStance = StanceRepository.getInstance(in.readLine());
				validStance = duellist1.changeStance(chosenStance);
			}

			System.out.println(String.format("%s switches to %s stance", 
				duellist1.getName(), chosenStance.getStanceType()
			));
			System.out.println();
			System.out.println(availableFormatAttack(duellist2.getAvailableAttacks(), duellist2.getName()));

			Attack chosenAttack = null;
			boolean validAttack = false;

			while(!validAttack){
				System.out.print("Please enter next attack : ");
				chosenAttack = Attack.getInstance(in.readLine());
				validAttack = duellist2.attackDuellist(chosenAttack, duellist1);
			}

			System.out.println(String.format("%s attacks %s with %s", duellist2.getName(), duellist1.getName(), chosenAttack.getType()));
			System.out.println();

			System.out.println(duellist1);
			System.out.println(duellist2);
			System.out.println();
			i++;
		}
	}

	private static String availableFormatAttack(List<Attack> list, String duellistName){
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("%s --- available attacks : ", duellistName)).append("\n");
		StringBuilder looperSb = new StringBuilder();
		for(Attack obj : list){
			looperSb.append(obj.toString()).append("\n");
		}

		sb.append(looperSb.toString());
		return sb.toString();
	}

	private static String availableFormatStances(List<Stance> list, String duellistName){
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("%s --- available stances : ", duellistName)).append("\n");
		StringBuilder looperSb = new StringBuilder();
		for(Stance obj : list){
			looperSb.append(obj.toString()).append("\n");
		}

		sb.append(looperSb.toString());
		return sb.toString();
	}
}