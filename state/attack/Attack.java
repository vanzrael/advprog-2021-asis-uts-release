package attack;

import java.util.Arrays;

public enum Attack {
	ATTACK("Attack", "Regular attack"), 
	SECOND_INTENTION("Second Intention", "An attack repertoire used to outmaneuver parries"), 
	RIPOSTE("Riposte", "An attack done immediately after a successful parry"), 
	FAKE("Fake", "A repertoire used to lure enemy into a unfavorable state"), 
	LUNGE("Lunge", "An explosive long range attack done by fully extending the arm forward");

	private String type;
	private String description;

	private Attack(String type, String description){
		this.type = type;
		this.description = description;
	}

	public String getType(){
		return type;
	}

	public String getDescription(){
		return description;
	}

	@Override
	public String toString(){
		return String.format("%s : %s", type, description);
	}

	private static final String[] ALL_ATTACKS_STR = {"Attack", "Second Intention", "Riposte", "Fake", "Lunge"};
	private static final Attack[] ALL_ATTACKS = {ATTACK, SECOND_INTENTION, RIPOSTE, FAKE, LUNGE};

	public static Attack getInstance(String type){
		Attack res = null;
		for(int i = 0; res == null && i < ALL_ATTACKS_STR.length; i++){
			res = type.equalsIgnoreCase(ALL_ATTACKS_STR[i]) ? ALL_ATTACKS[i] : null;
		}

		return res;
	}
}