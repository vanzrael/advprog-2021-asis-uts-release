# Soal State Pattern

Pada soal latihan kali ini Anda diminta untuk mengimplementasikan State Pattern dengan benar.

## Alur Program
Terdapat dua orang `Duellist` yang sedang bertarung dalam sebuah duel. Masing-masing *duellist* akan bergantian untuk menyerang dan bertahan. *Duellist* yang bertahan dapat memilih untuk menganti `Stance` (mengganti nilai `currentStance`) sedangkan *duellist* yang menyerang dapat memilih jenis serangan (`Attack`) yang ingin digunakan untuk *duellist* yang bertahan. Pilihan `Stance` untuk *duellist* bertahan dan `Attack` untuk  *duelllist* penyerang ditentukan oleh `Stance` yang digunakan oleh masing-masing *duellist* saat itu. (Pilihan `Stance` dan `Attack` sudah didefinisikan pada kode sehingga Anda tidak perlu mendefinisikan ulang)

Setelah fase penyerangan oleh *duellist* penyerang terhadap *duellist* bertahan selesai, masing-masing *duellist* akan mengalami pergantian `Stance` (penjelasan mengenai pergantian `Stance` setelah fase serangan selesai akan dijelaskan di bagian berikutnya). Kemudian, masing-masing *duellist* akan berganti peran (*duellist* penyerang akan menjadi *duellist* bertahan dan dapat melakukan pergantian `Stance` sesuai `Stance`, *duellist* bertahan akan menjadi *duellist* penyerang dan dapat memilih jenis `Attack`). Proses ini akan terus terjadi sampai `currentStance` dari salah satu *duellist* bernilai `Lost`.

![](assets/Avola_vs_Garozzo_2019_CIP.jpeg)
Caption : Pertanding kompetisi anggar *Challenge International de Paris* tahun 2019 antara [Daniele Garozzo (ITA, kiri)](https://en.wikipedia.org/wiki/Daniele_Garozzo) melawan [Giorgio Avola (ITA, kanan)](https://en.wikipedia.org/wiki/Giorgio_Avola). Sumber : Kanal Youtube *Fédération Française d'Escrime*/Federasi Anggar Nasional Prancis [(video tertera)](https://youtu.be/v5p1VkV5hvo)

### Penjelasan transisi Stance setelah fase penyerangan selesai
Saat *duellist* penyerang menyerang *duellist* bertahan, baik *duellist* penyerang maupun *duellist* bertahan akan mengganti nilai dari atribut `currentStance` yang mereka miliki. Nilai `currentStance` baru untuk masing-masing *duellist* ditentukan oleh jenis `Attack` yang dipilih oleh *duellist* penyerang dan nilai `currentStance` dari *duellist* bertahan. Adapun nilai-nilai tersebut adalah sebagai berikut:

nomor. (jenis `Attack`, nilai `currentStance` bertahan) -> (nilai `currentStance` baru untuk penyerang, nilai `currentStance` baru untuk penyerang)
1. (`Attack`/`Riposte`/`Lunge`, `Engarde`) -> (`Engarde`, `Lost`)
2. (`Fake`/`SecondIntention`, `Engarde`) -> (`Engarde`, `Retreat`)
3. (`Attack`/`Riposte`/, `Retreat`) -> (`Unstable`, `Engarde`)
4. (`Fake`/`SecondIntention`, `Retreat`) -> (`Engarde`, `Engarde`)
5. (`Lunge`, `Retreat`) -> (`Engarde`, `Lost`)
6. (`Attack`/`Riposte`/`Lunge`, `Parry`) -> (`Unstable`, `Parry`)
7. (`Fake`, `Parry`) -> (`Engarde`, `Unstable`)
8. (`SecondIntention`, `Parry`) -> (`Engarde`, `Lost`)
9. (`Attack`/`Riposte`/`Lunge`/`SecondIntention`, `Unstable`) -> (`Engarde`, `Lost`)
10. (`Fake`, `Unstable`) -> (`Engarde`, `Engarde`)
11. (`Attack`/`Riposte`/`Lunge`/`SecondIntention`/`Fake`, `Lost`) -> (`Engarde`, `Lost`)

## Requirements
Saat ini, program untuk menangani fase penyerangan dan perubahan nilai `currentStance` setelah fase tersebut berhasil belum diimplementasikan. Implementasikan fitur tersebut dengan menggunakan State Pattern. 

Hint : Anda dapat memanfaatkan kelas-kelas seperti `StancePair` dan `StanceRepository` untuk mengimplementasikan fitur tersebut.

## Cara menjalankan program
1. Jalankan `javac StateMain.java`
2. Jalankan `java StateMain`
3. Masukkan *input* yang diminta oleh program
4. *Output* akan ditampilkan oleh program sesuai dengan *input* yang dimasukkan
5. Program akan berhenti setelah memenuhi kondisi tertentu (lihat Alur Program)
