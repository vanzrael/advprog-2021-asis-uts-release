package stance;

import java.util.Arrays;
import java.util.List;
import attack.Attack;
import stance.misc.StancePair;
import stance.misc.StanceRepository;

public class Engarde extends Stance {
	public Engarde(){
		super("En garde", "Default stance with high adaptability", Arrays.asList(
			Attack.ATTACK, Attack.SECOND_INTENTION, Attack.FAKE, Attack.LUNGE
		));
	}
}