package stance;

import java.util.Arrays;
import java.util.List;
import attack.Attack;
import stance.misc.StancePair;
import stance.misc.StanceRepository;

public class Lost extends Stance {
	public Lost(){
		super("Lost", "Checkmate. You lose the duel", Arrays.asList());
	}
}