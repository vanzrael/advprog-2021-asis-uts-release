package stance;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import attack.Attack;
import stance.misc.StancePair;
import stance.misc.StanceRepository;
import stance.misc.StanceTransitionHandler;

public class Unstable extends Stance {
	private boolean renewRandom;
	private List<Stance> currentStances;

	public Unstable(){
		super("Unstable", "An unfavorable stance that needs a lot of luck and atheleticism to reverse the momentum", 
			Arrays.asList(Attack.ATTACK, Attack.FAKE)
		);
		currentStances = null;
		renewRandom = true;
	}

	/*
	* Notes : Pastikan untuk mengganti nilai renewRandom menjadi true
	* setiap kali melakukan handle serangan musuh
	*/
	@Override
	public List<Stance> getAvailableStances(){
		List<Stance> res = currentStances;
		if(renewRandom){
			Random rng = new Random();
			int randomNumber = rng.nextInt(5);
			StanceTransitionHandler transitionHandler = StanceTransitionHandler.getInstance();
			res = randomNumber == 0 
				? transitionHandler.getAvailableStances("En garde") 
				: transitionHandler.getAvailableStances("Unstable");
			currentStances = res;
			renewRandom = false;
		}

		return res;
	}
}