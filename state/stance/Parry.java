package stance;

import java.util.Arrays;
import java.util.List;
import attack.Attack;
import stance.misc.StancePair;
import stance.misc.StanceRepository;

public class Parry extends Stance {
	public Parry(){
		super("Parry", "A defensive stance used for dealing immediate counter-offensive", 
			Arrays.asList(Attack.RIPOSTE, Attack.FAKE)
		);
	}
}