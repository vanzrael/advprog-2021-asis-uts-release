package stance.misc;

import stance.Stance;

public class StancePair {
	private Stance attackerStance;
	private Stance defenderStance;

	public StancePair(Stance attackerStance, Stance defenderStance){
		this.attackerStance = attackerStance;
		this.defenderStance = defenderStance;
	}

	public Stance getAttackerStance(){
		return attackerStance;
	}

	public Stance getDefenderStance(){
		return defenderStance;
	}
}