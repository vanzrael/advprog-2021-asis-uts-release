package stance.misc;

import stance.*;

public class StanceRepository {
	public static final Stance ENGARDE = new Engarde();
	public static final Stance RETREAT = new Retreat();
	public static final Stance PARRY = new Parry();
	public static final Stance UNSTABLE = new Unstable();
	public static final Stance LOST = new Lost();

	private static final Stance[] ALL_STANCES = {ENGARDE, PARRY, RETREAT, UNSTABLE, LOST};
	private static final String[] ALL_STANCES_STR = {
		ENGARDE.getStanceType(), PARRY.getStanceType(), RETREAT.getStanceType(),
		UNSTABLE.getStanceType(), LOST.getStanceType()
	};

	public static Stance getInstance(String type){
		Stance res = null;
		for(int i = 0; res == null && i < ALL_STANCES_STR.length; i++){
			res = type.equalsIgnoreCase(ALL_STANCES_STR[i]) ? ALL_STANCES[i] : null;
		}

		return res;
	}
}