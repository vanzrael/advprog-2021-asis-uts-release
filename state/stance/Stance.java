package stance;

import java.util.List;
import attack.Attack;
import stance.misc.StancePair;
import stance.misc.StanceTransitionHandler;

public abstract class Stance {
	private String stanceType;
	private String description;
	private List<Stance> availableStances;
	private List<Attack> availableAttacks;

	public Stance(String stanceType, String description, List<Attack> availableAttacks){
		this.stanceType = stanceType;
		this.description = description;
		this.availableAttacks = availableAttacks;
		this.availableStances = null;
	}

	public boolean containsAttack(Attack attack){
		return availableAttacks.contains(attack);
	}

	public boolean containsStance(Stance stance){
		return getAvailableStances().contains(stance);
	}

	public List<Stance> getAvailableStances(){
		if(availableStances == null){
			availableStances = StanceTransitionHandler.getInstance().getAvailableStances(stanceType);
		}
		return availableStances;
	}

	public List<Attack> getAvailableAttacks(){
		return availableAttacks;
	}

	public String getStanceType(){
		return stanceType;
	}

	@Override
	public String toString(){
		return String.format("%s : %s", stanceType, description);
	}
}