public class Log {
    double durasi;
    String deskripsi;
    String lokasi;
    MataKuliah mataKuliah;

    public Log(MataKuliah mataKuliah, double durasi, String deskripsi, String lokasi) {
        this.durasi = durasi;
        this.deskripsi = deskripsi;
        this.mataKuliah = mataKuliah;
        this.lokasi = lokasi;
    }
}
