import java.util.ArrayList;

public class AsdosLog {
    String nama;
    String npm;
    int semesterKuliah;
    String tingkatan;
    ArrayList<Log> daftarLog;

    public AsdosLog(String nama, String npm, int semesterKuliah, String tingkatan){
        this.daftarLog = new ArrayList<>();
        this.nama = nama;
        this.npm = npm;
        this.semesterKuliah = semesterKuliah;
        
        if (tingkatan.equals("Mahasiswa S1")) {
            this.tingkatan = tingkatan;
        } else if (tingkatan.equals("Lulus S1")) {
            this.tingkatan = tingkatan;
        } else if (tingkatan.equals("Lulus S2")) {
            this.tingkatan = tingkatan;
        } else if (tingkatan.equals("Lulus S3")) {
            this.tingkatan = tingkatan;
        }
    }

    public void setTingkatan(String tingkatan) {
        if (tingkatan.equals("Mahasiswa S1")) {
            this.tingkatan = tingkatan;
        } else if (tingkatan.equals("Lulus S1")) {
            this.tingkatan = tingkatan;
        } else if (tingkatan.equals("Lulus S2")) {
            this.tingkatan = tingkatan;
        } else if (tingkatan.equals("Lulus S3")) {
            this.tingkatan = tingkatan;
        }
    }

    public void tambahLog(String namaMatkul, String kodeMatkul, double durasi, String deskripsi, String lokasi) {
        MataKuliah mataKuliah = new MataKuliah(namaMatkul, kodeMatkul);
        Log log = new Log(mataKuliah, durasi, deskripsi, lokasi);
        daftarLog.add(log);
    }

    public double hitungGaji() {
        double durasiTotal = 0;
        for (Log log : this.daftarLog) {
            durasiTotal += log.durasi;
        }

        if (tingkatan.equals("Mahasiswa S1")) {
            durasiTotal =  25000 * durasiTotal;
        } else if (tingkatan.equals("Lulus S1")) {
            durasiTotal =  30000 * durasiTotal;
        } else if (tingkatan.equals("Lulus S2")) {
            durasiTotal =  35000 * durasiTotal;
        } else if (tingkatan.equals("Lulus S3")) {
            durasiTotal =  40000 * durasiTotal;
        }

        return durasiTotal;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("nama : %s", nama)).append("\n");
        sb.append(String.format("tipe asdos : asdos log")).append("\n");
        sb.append(String.format("NPM : %s", npm)).append("\n");
        sb.append(String.format("semester kuliah : %d", semesterKuliah)).append("\n");
        sb.append(String.format("tingkatan : %s", tingkatan)).append("\n");
        sb.append(String.format("Total gaji : %f", hitungGaji())).append("\n");
        sb.append("=======================");
        return sb.toString();
    }
}
