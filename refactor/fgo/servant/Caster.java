package servant;

import java.util.Random;

public class Caster extends Servant {
	private static String[] effects = {"ineffective", "regular", "effective", "super effective"};
	private Random rng;

	public Caster(String name){
		super(name);
		this.type = "Caster";
		rng = new Random();
	}

	@Override
	public String getType(){
		return "Caster";
	}

	@Override
	public String attackWithBuster(){
		int selector = rng.nextInt(4);
		return String.format("%s attacks with buster. Effects : %s", name, effects[selector]);
	}

	@Override
	public String attackWithQuick(){
		int selector = rng.nextInt(4);
		return String.format("%s attacks with quick. Effects : %s", name, effects[selector]);
	}

	@Override
	public String attackWithArts(){
		int selector = rng.nextInt(4);
		return String.format("%s attacks with arts. Effects : %s", name, effects[selector]);
	}
}