package servant;

public class Saber extends Servant {
	public Saber(String name){
		super(name);
		this.type = "Saber";
	}

	@Override
	public String getType(){
		return "Saber";
	}

	@Override
	public String attackWithBuster(){
		return String.format("%s attacks with buster. Effects : regular", name);
	}

	@Override
	public String attackWithQuick(){
		return String.format("%s attacks with quick. Effects : regular", name);
	}

	@Override
	public String attackWithArts(){
		return String.format("%s attacks with arts. Effects : effective", name);
	}
}