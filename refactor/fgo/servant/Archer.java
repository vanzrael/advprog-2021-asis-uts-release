package servant;

public class Archer extends Servant {
	public Archer(String name){
		super(name);
		this.type = "Archer";
	}

	@Override
	public String getType(){
		return "Archer";
	}

	@Override
	public String attackWithBuster(){
		return String.format("%s attacks with buster. Effects : effective", name);
	}

	@Override
	public String attackWithQuick(){
		return String.format("%s attacks with quick. Effects : regular", name);
	}

	@Override
	public String attackWithArts(){
		return String.format("%s attacks with arts. Effects : regular", name);
	}
}