package servant;

public class Assassin extends Servant {
	public Assassin(String name){
		super(name);
		this.type = "Assassin";
	}

	@Override
	public String getType(){
		return "Assassin";
	}

	@Override
	public String attackWithBuster(){
		return String.format("%s attacks with buster. Effects : ineffective", name);
	}

	@Override
	public String attackWithQuick(){
		return String.format("%s attacks with quick. Effects : super effective", name);
	}

	@Override
	public String attackWithArts(){
		return String.format("%s attacks with arts. Effects : regular", name);
	}
}