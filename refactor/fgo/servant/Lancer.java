package servant;

public class Lancer extends Servant {
	public Lancer(String name){
		super(name);
		this.type = "Lancer";
	}

	@Override
	public String getType(){
		return "Lancer";
	}

	@Override
	public String attackWithBuster(){
		return String.format("%s attacks with buster. Effects : regular", name);
	}

	@Override
	public String attackWithQuick(){
		return String.format("%s attacks with quick. Effects : effective", name);
	}

	@Override
	public String attackWithArts(){
		return String.format("%s attacks with arts. Effects : regular", name);
	}
}