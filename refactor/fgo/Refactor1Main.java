import servant.*;
import java.util.Arrays;

public class Refactor1Main {
	public static void main(String[] args){
		Servant[] servants = {
			new Saber("Arturia"),
			new Lancer("Diarmuid"),
			new Archer("Gil"),
			new Assassin("Hassan"),
			new Rider("Alex"),
			new Berserker("Hercuteles"),
			new Caster("BAMBANG MAGIC")
		};

		int n = servants.length;
		for(int i = 0; i < n; i++){
			System.out.println(servants[i]);
		}
		System.out.println();

		for(int i = 0; i < n; i++){
			System.out.println(servants[i].attackPattern());
		}
	}
}